﻿using System;
using System.Collections.Generic;
using System.Web.Routing;
using Nop.Core;
using System.Linq;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.Pesapal.Controllers;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Tax;
using Pesapal.APIHelper;
using System.Web.Mvc;
using System.Web;

namespace Nop.Plugin.Payments.Pesapal
{
    public class PesapalPaymentHelper : BasePlugin, IPesapal
    {
        #region Fields

        private readonly CurrencySettings _currencySettings;
        private readonly ICheckoutAttributeParser _checkoutAttributeParser;
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly Services.Payments.IPaymentService _paymentService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly ITaxService _taxService;
        private readonly IWebHelper _webHelper;
        private readonly PesapalPaymentSettings _pesapalPaymentSettings;

        #endregion

        #region Ctor

        public PesapalPaymentHelper(CurrencySettings currencySettings,
            ICheckoutAttributeParser checkoutAttributeParser,
            ICurrencyService currencyService,
            ICustomerService customerService,
            ILocalizationService localizationService,
            IOrderTotalCalculationService orderTotalCalculationService,
            Services.Payments.IPaymentService paymentService,
            IPriceCalculationService priceCalculationService,
            IProductAttributeParser productAttributeParser,
            ISettingService settingService,
            IStoreContext storeContext,
            ITaxService taxService,
            IWebHelper webHelper,
            PesapalPaymentSettings pesapalPaymentSettings)
        {
            this._currencySettings = currencySettings;
            this._checkoutAttributeParser = checkoutAttributeParser;
            this._currencyService = currencyService;
            this._customerService = customerService;
            this._localizationService = localizationService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._paymentService = paymentService;
            this._priceCalculationService = priceCalculationService;
            this._productAttributeParser = productAttributeParser;
            this._settingService = settingService;
            this._storeContext = storeContext;
            this._taxService = taxService;
            this._webHelper = webHelper;
            this._pesapalPaymentSettings = pesapalPaymentSettings;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Gets a payment status
        /// </summary>
        /// <param name="state">PayPal state</param>
        /// <returns>Payment status</returns>
        public PaymentStatus GetPaymentStatus(string state)
        {
            state = state ?? string.Empty;
            var result = PaymentStatus.Pending;

            switch (state.ToLowerInvariant())
            {
                case "pending":
                    result = PaymentStatus.Pending;
                    break;
                case "completed":
                    result = PaymentStatus.Paid;
                    break;
                case "reversed":
                    result = PaymentStatus.Refunded;
                    break;
                case "invalid":
                    result = PaymentStatus.Voided;
                    break;
                default:
                    break;
            }

            return result;
        }

        public string CheckPaymentStatus(string consumerKey, string consumerSecret, string pesapal_tracking_id, string pesapal_merchant_ref, bool is_demo)
        {
            string statusUrl = "https://www.pesapal.com";
            if (is_demo)
                statusUrl = "https://demo.pesapal.com";
            statusUrl += "/api/querypaymentstatus";
            Uri pesapalQueryPaymentStatusUri = new Uri(statusUrl);
            IBuilder builder = new APIPostParametersBuilder().ConsumerKey(consumerKey).ConsumerSecret(consumerSecret).OAuthVersion(EOAuthVersion.VERSION1).SignatureMethod(ESignatureMethod.HMACSHA1).SimplePostHttpMethod(EHttpMethod.GET).SimplePostBaseUri(pesapalQueryPaymentStatusUri);
            // Initialize API helper     
            var helper = new APIHelper<IBuilder>(builder);
            string result = helper.PostGetQueryPaymentStatus(pesapal_tracking_id, pesapal_merchant_ref);
            string[] resultParts = result.Split(new char[] { '=' });
            string paymentStatus = resultParts[1];
            return paymentStatus;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            return result;
        }
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
        }

            /// <summary>
            /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
            /// </summary>
            /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
            public string PostProcessPaymentIframe(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            var customer = _customerService.GetCustomerById(postProcessPaymentRequest.Order.CustomerId);
            var storeScope = _storeContext.CurrentStore.Id;
            var pesapalPaymentSettings = _settingService.LoadSetting<PesapalPaymentSettings>(storeScope);
            if (customer == null)
                throw new Exception("Customer cannot be loaded");
            //currency
            var currency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            var order = postProcessPaymentRequest.Order;
            //get current shopping cart
            var shoppingCart = customer.ShoppingCartItems
                .Where(shoppingCartItem => shoppingCartItem.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(order.StoreId).ToList();
           
            PesapalDirectOrderInfo webOrder = new PesapalDirectOrderInfo()
            {
                Amount = order.OrderTotal.ToString(),
                AccountNumber = order.Id.ToString(),
                Description = customer.Email+":"+ order.Id.ToString(),
                Type = "MERCHANT",
                Currency = HttpUtility.UrlEncode(_currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode),
                Reference = order.OrderGuid.ToString(),
                Email = customer.Email,
                FirstName = customer.BillingAddress.FirstName,
                LastName = customer.BillingAddress.LastName,
                PhoneNumber = customer.BillingAddress.PhoneNumber
            };
            string iframeUrl = "https://www.pesapal.com";
            if (pesapalPaymentSettings.UseSandbox)
                iframeUrl = "https://demo.pesapal.com";
            iframeUrl += "/API/PostPesapalDirectOrderV4";
            string callbackUrl = BaseUrl+"Plugins/PaymentPesapal/CompleteOrder";
            IBuilder builder = new APIPostParametersBuilderV2()
                .ConsumerKey(pesapalPaymentSettings.ClientId)
                .ConsumerSecret(pesapalPaymentSettings.ClientSecret)
                .OAuthVersion(EOAuthVersion.VERSION1)
                .SignatureMethod(ESignatureMethod.HMACSHA1)
                .SimplePostHttpMethod(EHttpMethod.GET)
                .SimplePostBaseUri(new Uri(iframeUrl))
                .OAuthCallBackUri(new Uri(callbackUrl));
            APIHelper<IBuilder> helper = new APIHelper<IBuilder>(builder);
            return helper.PostGetPesapalDirectOrderUrl(webOrder);
        }
        public static string BaseUrl
        {
            get
            {
                // variables  
                string Authority = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority).TrimStart('/').TrimEnd('/');
                string ApplicationPath = HttpContext.Current.Request.ApplicationPath.TrimStart('/').TrimEnd('/');

                // add trailing slashes if necessary  
                if (Authority.Length > 0)
                {
                    Authority += "/";
                }

                if (ApplicationPath.Length > 0)
                {
                    ApplicationPath += "/";
                }

                // return  
                return string.Format("{0}{1}", Authority, ApplicationPath);
            }
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            return false;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {

            return 0;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.NewPaymentStatus = PaymentStatus.Pending;
            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Core.Domain.Orders.Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //it's not a redirection payment method. So we always return false
            return false;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentPesapal";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Pesapal.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Gets a route for payment info
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentPesappal";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Pesapal.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Get type of controller
        /// </summary>
        /// <returns>Type</returns>
        public Type GetControllerType()
        {
            return typeof(PaymentPesapalController);
        }

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            //settings
            var settings = new PesapalPaymentSettings
            {
                UseSandbox = true,
            };
            _settingService.SaveSetting(settings);
            var desc = "<div class=\"row\"><div class=\"column\">" +
                "<img src=\"/Plugins/Payments.Pesapal/assets/visa.png\" alt=\"Visa\" title=\"Visa\" />" +
                "<img src=\"/Plugins/Payments.Pesapal/assets/mastercard.png\" alt=\"MasterCard\" title=\"MasterCard\" />" +
                "<img src=\"/Plugins/Payments.Pesapal/assets/americanexpress.png\" alt=\"American Express\" title=\"American Express\" />" +
                "<img src=\"/Plugins/Payments.Pesapal/assets/mpesa.png\" alt=\"MPESA\" title=\"MPESA\" />" +
                "<img src=\"/Plugins/Payments.Pesapal/assets/airtel.png\" alt=\"Airtel Money\" title=\"Airtel Money\" />" +
                "<img src=\"/Plugins/Payments.Pesapal/assets/equity.png\" alt=\"Equity\" title=\"Equity\" />" +
                "<img src=\"/Plugins/Payments.Pesapal/assets/coopbank.png\" alt=\"Co-op Bank\" title=\"Co-op Bank\" />" +
                "<img src=\"/Plugins/Payments.Pesapal/assets/ewallet.png\" alt=\"E wallet\" title=\"E wallet\" />" +
                "</div></div>";
            desc = "";
            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.ClientId", "Client ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.ClientId.Hint", "Specify client ID.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.ClientSecret", "Client secret");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.ClientSecret.Hint", "Specify secret key.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.PassPurchasedItems", "Pass purchased items");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.PassPurchasedItems.Hint", "Check to pass information about purchased items to Pesapal.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.UseSandbox", "Use Sandbox");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.UseSandbox.Hint", "Check to enable Sandbox (testing environment).");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.IpnUrl", "IPN URL");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.Fields.IpnUrl.Hint", "IpnUrl To Be Set In merchant dashboard");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pesapal.PaymentMethodDescription", HttpUtility.HtmlEncode(desc));
            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            //delete webhook
            var settings = _settingService.LoadSetting<PesapalPaymentSettings>();

            //settings
            _settingService.DeleteSetting<PesapalPaymentSettings>();

            //locales
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.ClientId");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.ClientId.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.ClientSecret");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.ClientSecret.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.PassPurchasedItems");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.PassPurchasedItems.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.UseSandbox");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.UseSandbox.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.IpnUrl");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.IpnUrl.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Pesapal.Fields.IpnUrl.PaymentMethodDescription");
            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get { return RecurringPaymentType.Automatic; }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get { return PaymentMethodType.Redirection; }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a payment method description that will be displayed on checkout pages in the public store
        /// </summary>
        public string PaymentMethodDescription
        {
            //return description of this payment method to be display on "payment method" checkout step. good practice is to make it localizable
            //for example, for a redirection payment method, description may be like this: "You will be redirected to PayPal site to complete the payment"
            get { return _localizationService.GetResource("Plugins.Payments.Pesapal.PaymentMethodDescription"); }
        }

        #endregion
    }
}
