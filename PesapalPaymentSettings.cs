﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.Pesapal
{
    /// <summary>
    /// PayPal Direct payment settings
    /// </summary>
    public class PesapalPaymentSettings : ISettings
    {
        /// <summary>
        /// Gets or sets the client id
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Gets or sets the secret key
        /// </summary>
        public string ClientSecret { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use sandbox (testing environment)
        /// </summary>
        public bool UseSandbox { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to pass info about purchased items to PayPal
        /// </summary>
        public string IpnUrl { get; set; }
        public bool PassPurchasedItems { get; set; } //
    }
}
