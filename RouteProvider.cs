﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Payments.Pesapal
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            //Callback
            routes.MapRoute("Plugin.Payments.Pesapal.CompleteOrder",
                 "Plugins/PaymentPesapal/CompleteOrder",
                 new { controller = "PaymentPesapal", action = "CompleteOrder" },
                 new[] { "Nop.Plugin.Payments.Pesapal.Controllers" }
            );
            routes.MapRoute("Plugin.Payments.Pesapal.IPN",
                 "Plugins/PaymentPesapal/Ipn",
                 new { controller = "PaymentPesapal", action = "Ipn" },
                 new[] { "Nop.Plugin.Payments.Pesapal.Controllers" }
            );
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
