﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.Payments;
using Nop.Core.Plugins;

namespace Nop.Plugin.Payments.Pesapal
{
    public partial interface IPesapal : IPaymentMethod
    {
        /// <summary>
        /// Post process payment and return iframe
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        string PostProcessPaymentIframe(PostProcessPaymentRequest postProcessPaymentRequest);
    }
}
