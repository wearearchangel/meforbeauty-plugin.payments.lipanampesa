﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Services.Common;
using Nop.Data;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Pesapal.APIHelper;
using Nop.Plugin.Payments.Pesapal.Models;
namespace Nop.Plugin.Payments.Pesapal.Controllers
{
    public class PaymentPesapalController : BasePaymentController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;
        private readonly ISettingService _settingService;
        private readonly Services.Payments.IPaymentService _paymentService;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly PaymentSettings _paymentSettings;
        private readonly PesapalPaymentSettings _pesapalPaymentSettings;
        private readonly ShoppingCartSettings _shoppingCartSettings;

        public PaymentPesapalController(IWorkContext workContext,
            IStoreService storeService,
            ISettingService settingService,
            Services.Payments.IPaymentService paymentService,
            IOrderService orderService,
            IOrderProcessingService orderProcessingService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IStoreContext storeContext,
            ILogger logger,
            IWebHelper webHelper,
            PaymentSettings paymentSettings,
            PesapalPaymentSettings pesapalPaymentSettings,
            ShoppingCartSettings shoppingCartSettings)
        {
            this._workContext = workContext;
            this._storeService = storeService;
            this._settingService = settingService;
            this._paymentService = paymentService;
            this._orderService = orderService;
            this._orderProcessingService = orderProcessingService;
            this._genericAttributeService = genericAttributeService;
            this._localizationService = localizationService;
            this._storeContext = storeContext;
            this._logger = logger;
            this._webHelper = webHelper;
            this._paymentSettings = paymentSettings;
            this._pesapalPaymentSettings = pesapalPaymentSettings;
            this._shoppingCartSettings = shoppingCartSettings;
        }

        #endregion

        #region Methods

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var pesapalSettings = _settingService.LoadSetting<PesapalPaymentSettings>(storeScope);

            var model = new ConfigurationModel
            {
                ClientId = pesapalSettings.ClientId,
                ClientSecret = pesapalSettings.ClientSecret,
                UseSandbox = pesapalSettings.UseSandbox,
                IpnUrl = pesapalSettings.IpnUrl
            };
            if (storeScope > 0)
            {
                model.ClientId_OverrideForStore = _settingService.SettingExists(pesapalSettings, x => x.ClientId, storeScope);
                model.ClientSecret_OverrideForStore = _settingService.SettingExists(pesapalSettings, x => x.ClientSecret, storeScope);
                model.UseSandbox_OverrideForStore = _settingService.SettingExists(pesapalSettings, x => x.UseSandbox, storeScope);
            }
            ViewBag.Domain = PesapalPaymentHelper.BaseUrl;

            return View("~/Plugins/Payments.Pesapal/Views/Pesapal/Configure.cshtml", model);
        }

        [HttpPost, ActionName("Configure")]
        [FormValueRequired("save")]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var pesapalSettings = _settingService.LoadSetting<PesapalPaymentSettings>(storeScope);

            //save settings
            pesapalSettings.ClientId = model.ClientId;
            pesapalSettings.ClientSecret = model.ClientSecret;
            pesapalSettings.UseSandbox = model.UseSandbox;
            pesapalSettings.IpnUrl = model.IpnUrl;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
             if (model.ClientId_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(pesapalSettings, x => x.ClientId, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(pesapalSettings, x => x.ClientId, storeScope);
            if (model.ClientSecret_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(pesapalSettings, x => x.ClientSecret, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(pesapalSettings, x => x.ClientSecret, storeScope);
            if (model.UseSandbox_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(pesapalSettings, x => x.UseSandbox, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(pesapalSettings, x => x.UseSandbox, storeScope);
            if (model.IpnUrl_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(pesapalSettings, x => x.IpnUrl, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(pesapalSettings, x => x.IpnUrl, storeScope);
            /*_settingService.SaveSettingOverridablePerStore(pesapalSettings, x => x.ClientId, model.ClientId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(pesapalSettings, x => x.ClientSecret, model.ClientSecret_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(pesapalSettings, x => x.UseSandbox, model.UseSandbox_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(pesapalSettings, x => x.IpnUrl, model.IpnUrl_OverrideForStore, storeScope, false);*/

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

       

        [NonAction]
        public override IList<string> ValidatePaymentForm(FormCollection form)
        {
            var warnings = new List<string>();
            return warnings;
        }

        [NonAction]
        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();

            return paymentInfo;
        }
        [ChildActionOnly]
        public ActionResult PaymentInfo()
        {
            return View("~/Plugins/Payments.Pesapal/Views/PesapalPaymentInfo.cshtml");
        }
        [ValidateInput(false)]
        public ActionResult CompleteOrder(FormCollection form)
        {
            var pesapal_merchant_reference = _webHelper.QueryString<string>("pesapal_merchant_reference");
            var pesapal_transaction_tracking_id = _webHelper.QueryString<string>("pesapal_transaction_tracking_id");
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var pesapalPaymentSettings = _settingService.LoadSetting<PesapalPaymentSettings>(storeScope);
            var processor = _paymentService.LoadPaymentMethodBySystemName("Payments.Pesapal") as PesapalPaymentHelper;
            if (processor == null ||
                !processor.IsPaymentMethodActive(_paymentSettings) || !processor.PluginDescriptor.Installed)
                throw new NopException("Pesapal Standard module cannot be loaded");
                Guid orderNumberGuid = Guid.Empty;
                try
                {
                    orderNumberGuid = new Guid(pesapal_merchant_reference);
                }
                catch { }
                Nop.Core.Domain.Orders.Order order = _orderService.GetOrderByGuid(orderNumberGuid);
                if (order != null)
                {
                string payment_status = processor.CheckPaymentStatus(pesapalPaymentSettings.ClientId, pesapalPaymentSettings.ClientSecret, pesapal_transaction_tracking_id, pesapal_merchant_reference, pesapalPaymentSettings.UseSandbox);
                var newPaymentStatus = processor.GetPaymentStatus(payment_status);
                var sb = new StringBuilder();
                sb.AppendLine("New payment status: " + newPaymentStatus);
                order.PaymentStatus = newPaymentStatus;

                    //order note
                order.OrderNotes.Add(new OrderNote
                    {
                        Note = sb.ToString(),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);
                    //mark order as paid
                    if (newPaymentStatus == PaymentStatus.Paid)
                    {
                        if (_orderProcessingService.CanMarkOrderAsPaid(order))
                        {
                            order.AuthorizationTransactionId = pesapal_transaction_tracking_id;
                            _orderService.UpdateOrder(order);

                            _orderProcessingService.MarkOrderAsPaid(order);
                        }
                    }
                }

                return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
        }
            //noth

        #endregion
    }
}